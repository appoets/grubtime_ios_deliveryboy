//
//  StripeTokenEntity.swift
//  Project
//
//  Created by Prem's on 08/10/20.
//  Copyright © 2020 css. All rights reserved.
//

import Foundation

struct StripeTokenEntity : Codable {
    
    var status : Bool?
    var message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
