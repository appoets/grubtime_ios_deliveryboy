//
//  EarningsEntity.swift
//  Project
//
//  Created by Prem's on 18/10/20.
//  Copyright © 2020 css. All rights reserved.
//

import Foundation

struct EarningsEntity : Codable {
    
    var earnings : [EarinigsData]?

    enum CodingKeys: String, CodingKey {

        case earnings = "earnings"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        earnings = try values.decodeIfPresent([EarinigsData].self, forKey: .earnings)
    }

}

struct EarinigsData: Codable {
    
    var transporter_id: Int?
    var id: Int?
    var amount: String?
    var status: String?
    var comment: String?
}
