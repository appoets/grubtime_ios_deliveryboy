//
//  OrderDetailViewController.swift
//  Project
//
//  Created by CSS on 25/01/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var LineImageView: UIImageView!
    @IBOutlet weak var verifyImage: UIImageView!
    @IBOutlet weak var orderStatusHeaderLabel: UILabel!
    @IBOutlet var orderIdHeaderLabel: UILabel!
    @IBOutlet weak var btnBackButton: UIButton!
    @IBOutlet weak var paymenttypeLabel: UILabel!
    @IBOutlet weak var paymentModeLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var orderItemTableView: UITableView!
    @IBOutlet weak var orderTimeLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var deliveryStatusLabel: UILabel!
    @IBOutlet weak var restrauntAddressLabel: UILabel!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var homeAddressLabel: UILabel!
    @IBOutlet weak var restrauntNameLabel: UILabel!
    @IBOutlet weak var homeImageView: UIImageView!
    @IBOutlet weak var verticalImageView: UIImageView!
    @IBOutlet weak var pinImageView: UIImageView!
    @IBOutlet weak var OverView: UIView!
    @IBOutlet weak var itemTotalLabel: UILabel!
    
    @IBOutlet weak var returnedValueLabel: UILabel!
    @IBOutlet weak var returnedLabel: UILabel!
    @IBOutlet weak var deliveryChargesValueLabel: UILabel!
    @IBOutlet weak var deliveryChargesLabel: UILabel!
    @IBOutlet weak var billAmountValueLabel: UILabel!
    
    @IBOutlet weak var billPaidValueLabel: UILabel!
    @IBOutlet weak var billPaidLabel: UILabel!
    @IBOutlet weak var billAmountLabel: UILabel!
    @IBOutlet weak var noOfItemLabel: UILabel!
    @IBOutlet weak var noOfItemValueLabel: UILabel!

    @IBOutlet weak var walletDeductionLabel: UILabel!
    @IBOutlet weak var walletDeductionValueLabel: UILabel!
    @IBOutlet weak var discountValueLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var serviceTaxValueLabel: UILabel!
    @IBOutlet weak var serviceTaxLabel: UILabel!
    @IBOutlet weak var itemTotalValueLabel: UILabel!
    
    //Height Contraints For TableView
    @IBOutlet weak var orderItemHeight: NSLayoutConstraint!
    
    @IBOutlet var walletDeductionStackView: UIStackView!
    @IBOutlet var serviceTaxStackView: UIStackView!
    
    var Data: OrdersEntity?

// MARK: - View Life cycle
    
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initalLoad()
    }
    
    
    // viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
     //viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false

    }
    
    // MARK: -  Button Action
    
    // back button Action
    @IBAction func backbtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - Methods
extension OrderDetailViewController {
    
    // InitalLoads method
    func initalLoad(){
        
        registerTableview()
        setFont()
        setColors()
        setData(data: Data!)
    }
    // Register TableView
    private func registerTableview(){
        let orderItemnib = UINib(nibName: XIB.Names.itemListTableViewCell, bundle: nil)
        orderItemTableView.register(orderItemnib, forCellReuseIdentifier: XIB.Names.itemListTableViewCell)
        let addonsnib = UINib(nibName: XIB.Names.ItemWithoutAddOnsTableViewCell, bundle: nil)
        orderItemTableView.register(addonsnib, forCellReuseIdentifier: XIB.Names.ItemWithoutAddOnsTableViewCell)
        orderItemTableView.delegate = self
        orderItemTableView.dataSource = self
    }
    
    // set Font
    private func setFont(){
        paymenttypeLabel.font = UIFont.regular(size: 14)
        paymentModeLabel.font = UIFont.regular(size: 14)
        totalAmountLabel.font = UIFont.regular(size: 14)
        totalLabel.font = UIFont.regular(size: 14)
        orderIdLabel.font = UIFont.bold(size: 14)
        orderTimeLabel.font = UIFont.regular(size: 14)
        deliveryStatusLabel.font = UIFont.regular(size: 15)
        restrauntNameLabel.font = UIFont.bold(size: 14)
        restrauntAddressLabel.font = UIFont.regular(size: 14)
        homeLabel.font = UIFont.bold(size: 14)
        homeAddressLabel.font = UIFont.regular(size: 14)
        orderIdHeaderLabel.font = UIFont.bold(size: 16)
        orderStatusHeaderLabel.font = UIFont.regular(size: 12)
        itemTotalLabel.font = UIFont.regular(size: 13)
        itemTotalValueLabel.font = UIFont.regular(size: 13)
        serviceTaxLabel.font = UIFont.regular(size: 13)
        serviceTaxValueLabel.font = UIFont.regular(size: 13)
        deliveryChargesLabel.font = UIFont.regular(size: 13)
        deliveryChargesValueLabel.font = UIFont.regular(size: 13)
        discountLabel.font = UIFont.regular(size: 13)
        discountValueLabel.font = UIFont.regular(size: 13)
        walletDeductionLabel.font = UIFont.regular(size: 13)
        walletDeductionValueLabel.font = UIFont.regular(size: 13)
        noOfItemLabel.font = UIFont.regular(size: 13)
        noOfItemValueLabel.font = UIFont.regular(size: 13)
        billPaidLabel.font = UIFont.regular(size: 13)
        billAmountLabel.font = UIFont.regular(size: 13)
        billPaidValueLabel.font = UIFont.regular(size: 13)
        billAmountValueLabel.font = UIFont.regular(size: 13)
        returnedLabel.font = UIFont.regular(size: 13)
        returnedValueLabel.font = UIFont.regular(size: 13)
    }
    
    //  update TableView Height
    private func updateOrderItemTableHeight(){
        var counts: [String: Int] = [:]
        var itemsArr = [String]()
        for var i in 0..<(Data!.items!.count)
        {
            let Result = Data!.items![i]
            
            let cartaddons = Result.cart_addons?.count
            if cartaddons! > 0 {
                itemsArr.append("withaddonsItems")
            }else{
                itemsArr.append("withoutaddonsItems")

            }
        }
        for item in itemsArr {
            counts[item] = (counts[item] ?? 0) + 1
        }
        var cartaddonCount = 0
        var itemCount = 0
        for (key, value) in counts {
            print("\(key) occurs \(value) time(s)")
            if key == "withoutaddonsItems"{
                itemCount = value
            }else{
                cartaddonCount = value
            }
        }
        
        let itemCountHeight = CGFloat(itemCount * 44)
        let cartaddOns = CGFloat(cartaddonCount * 80)
        self.orderItemHeight.constant = orderItemTableView.contentSize.height + 20 //itemCountHeight + cartaddOns
        scrollView.contentSize = CGSize(width: self.OverView.frame.size.width, height:  OverView.frame.size.height)

    }
    

    // Set Value for the textFields
    func setData(data: OrdersEntity){
        
        
        let itemCount = data.items?.map{$0.quantity ?? 0}.reduce(0, +)
        let itemCountStr = "\(itemCount ?? 0)" //"\(data.items!.count)"
        let billAmountStr = "\(Double(data.invoice!.net ?? 0.00).twoDecimalPoint )"
        let billPaidStr = "\(Double(data.invoice!.total_pay ?? 0.00).twoDecimalPoint)"
        let returnAmountStr = "\(Double(data.invoice!.tender_pay ?? 0).twoDecimalPoint)"
        
        let currencyBillAmtStr = APPLocalize.localizestring.Currency.localize() + billAmountStr
        let currencyBillPaidStr = APPLocalize.localizestring.Currency.localize() + billPaidStr
        let currencyReturnAmountStr = APPLocalize.localizestring.Currency.localize() + returnAmountStr
        
        noOfItemValueLabel.text = itemCountStr
        billAmountValueLabel.text = currencyBillAmtStr
        billPaidValueLabel.text = currencyBillPaidStr
        returnedValueLabel.text = currencyReturnAmountStr
        
        let itemTotalStr = "\(Double(data.invoice!.gross ?? 0.00).twoDecimalPoint )"
        //let serviceTaxStr = "\(Double(data.invoice!.tax ?? 0).twoDecimalPoint)"
        let promoAmount = "\(data.invoice?.promocode_amount ?? 0.00)"
        
        let deliveryChargesStr = (data.invoice?.delivery_charge ?? 0.00) > 0 ? "\((data.invoice!.delivery_charge ?? 0.00).twoDecimalPoint)" : APPLocalize.localizestring.free.localize()
        let discountStr = "\((data.invoice!.discount ?? 0.00).twoDecimalPoint)"
        let walletDeductionStr = "\(Double(data.invoice!.wallet_amount ?? 0).twoDecimalPoint)"
        let currencyItemTotalStr = APPLocalize.localizestring.Currency.localize() + itemTotalStr
        //let currencyServiceTaxStr = APPLocalize.localizestring.Currency.localize() + serviceTaxStr
        let currencyPromoStr = "-" + APPLocalize.localizestring.Currency.localize() + promoAmount
        let currencyDeliveryChargesStr = deliveryChargesStr == APPLocalize.localizestring.free.localize() ? deliveryChargesStr : APPLocalize.localizestring.Currency.localize() + deliveryChargesStr
        let currencyDiscountStr = "-" + APPLocalize.localizestring.Currency.localize() + discountStr
        let currencyWalletDeductionStr = "-" + APPLocalize.localizestring.Currency.localize() + walletDeductionStr
        
        itemTotalValueLabel.text = currencyItemTotalStr
        serviceTaxValueLabel.text = currencyPromoStr //currencyServiceTaxStr
        deliveryChargesValueLabel.text = currencyDeliveryChargesStr
        deliveryChargesValueLabel.textColor = currencyDeliveryChargesStr == APPLocalize.localizestring.free.localize() ? .orderStatusSuccessColor : .descDarkColor
        discountValueLabel.text = currencyDiscountStr
        walletDeductionValueLabel.text = currencyWalletDeductionStr
        walletDeductionLabel.text = APPLocalize.localizestring.oyolaCreditsApplied.localize()
        orderTimeLabel.text = data.created_at
        homeLabel.text = APPLocalize.localizestring.home.localize()
        
        discountLabel.text = APPLocalize.localizestring.kitchenDiscount.localize()
        
        let orderIdStr = "\(data.id!)"
        
        orderIdHeaderLabel.text = "ORDER #" + orderIdStr
        orderIdLabel.text = "#" + orderIdStr
        
        restrauntNameLabel.text = data.shop?.name?.uppercased()
        restrauntAddressLabel.text = data.shop?.maps_address
        homeAddressLabel.text = data.address?.map_address
        let orderStr = data.status
        orderStatusHeaderLabel.text = orderStr! + " | " + itemCountStr + "Item ,$" + billAmountStr
        
        if data.status == "COMPLETED" {
            deliveryStatusLabel.text = APPLocalize.localizestring.orderDeliveredSucess.localize()
            verifyImage.image = UIImage(named: ImageString.imageValue.checked)
        }else{
            deliveryStatusLabel.text = APPLocalize.localizestring.orderCancelled.localize()
            verifyImage.image = UIImage(named: ImageString.imageValue.ordercancelled)
            
        }
        
        totalLabel.text = APPLocalize.localizestring.total.localize()
        let totalAmtStr = "\((data.invoice!.payable ?? 0.0).twoDecimalPoint)"
        
        totalAmountLabel.text = APPLocalize.localizestring.Currency.localize() + totalAmtStr
        
        paymentModeLabel.text = APPLocalize.localizestring.paymentMode.localize()
        
        serviceTaxStackView.isHidden = (data.invoice?.promocode_amount ?? 0.0) > 0.0 ? false : true
        
        if data.invoice?.payment_mode == "stripe" || data.invoice?.payment_mode == "braintree" {
            paymenttypeLabel.text = APPLocalize.localizestring.card.localize()
            walletDeductionStackView.isHidden = true
        }else if data.invoice?.payment_mode == "cash" {
            paymenttypeLabel.text = APPLocalize.localizestring.card.localize() //APPLocalize.localizestring.cash.localize()
            walletDeductionStackView.isHidden = true
        }else{
            paymenttypeLabel.text = APPLocalize.localizestring.oyolaCredits.localize() //APPLocalize.localizestring.card.localize()
            walletDeductionStackView.isHidden = false
        }
        
        orderItemTableView.reloadData()
        orderItemTableView.layoutIfNeeded()
        updateOrderItemTableHeight()
    }
    
    // Set Colors
    private func setColors() {
        paymenttypeLabel.textColor = UIColor.primary
        orderIdHeaderLabel.textColor = UIColor.baseColor
        orderTimeLabel.textColor = UIColor.descDarkColor
        deliveryStatusLabel.textColor = UIColor.orderStatusSuccessColor
        itemTotalLabel.textColor = UIColor.descDarkColor
        itemTotalValueLabel.textColor = UIColor.descDarkColor
        serviceTaxLabel.textColor = UIColor.orderStatusSuccessColor
        serviceTaxValueLabel.textColor = UIColor.orderStatusSuccessColor
        deliveryChargesLabel.textColor = UIColor.descDarkColor
        deliveryChargesValueLabel.textColor = UIColor.descDarkColor
        discountLabel.textColor = UIColor.descDarkColor
        discountValueLabel.textColor = UIColor.descDarkColor
        walletDeductionLabel.textColor = UIColor.orderStatusSuccessColor
        walletDeductionValueLabel.textColor = UIColor.orderStatusSuccessColor
        noOfItemLabel.textColor = UIColor.descDarkColor
        noOfItemValueLabel.textColor = UIColor.descDarkColor
        billPaidLabel.textColor = UIColor.descDarkColor
        billAmountLabel.textColor = UIColor.descDarkColor
        billPaidValueLabel.textColor = UIColor.descDarkColor
        billAmountValueLabel.textColor = UIColor.descDarkColor
        returnedLabel.textColor = UIColor.primary
        returnedValueLabel.textColor = UIColor.primary
        orderIdLabel.textColor = UIColor.primary
    }

    
}
// MARK: - TableView Delegate & DataSoruce
extension OrderDetailViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data?.items?.count ?? 0
    }
    
  
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if Data!.items![indexPath.row].cart_addons!.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.ItemWithoutAddOnsTableViewCell, for: indexPath) as! ItemWithoutAddOnsTableViewCell
            
            /*let productName = Data?.items?[indexPath.row].product?.name
            let quantityStr = "\(Data!.items![indexPath.row].quantity!)"
            cell.titleLabel.text = productName! + " x " + quantityStr
            let priceStr = "\(Data!.items![indexPath.row].product!.prices!.orignal_price?.twoDecimalPoint ?? "")"
            cell.descriptionLabel.text = APPLocalize.localizestring.Currency.localize() + priceStr*/
            let selectedItem = Data?.items![indexPath.row]
            let productName = selectedItem?.product?.name
            let quantity1 = "\(selectedItem?.quantity ?? 0)"
            let currency = selectedItem?.product?.prices?.currency ?? "$"
            let priceStr: String! = String(describing: selectedItem?.product?.prices?.orignal_price ?? 0)
            let priceDouble: Double = Double(priceStr) ?? 0.0
            cell.titleLabel.text = "\(productName ?? "")(\(quantity1)x\(currency)\(String(format: "%.02f", priceDouble)))"
            let quantityStr = Double(selectedItem?.quantity ?? 0)
            let originalPrice = Double(selectedItem?.product?.prices?.orignal_price ?? 0)
            let value =   originalPrice * quantityStr
            cell.descriptionLabel.text = currency + String(format: " %.02f", Double(value))
            
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.ItemListTableViewCell, for: indexPath) as! ItemListTableViewCell
            
            
            /*let productName = Data?.items?[indexPath.row].product?.name
            let quantityStr = "\(Data!.items![indexPath.row].quantity!)"
            cell.titleLabel.text = productName! + " x " + quantityStr
            let priceStr = "\(Data!.items![indexPath.row].product!.prices!.orignal_price?.twoDecimalPoint ?? "")"
            cell.descriptionLabel.text = APPLocalize.localizestring.Currency.localize() + priceStr
            var addonsNameArr = [String]()
            addonsNameArr.removeAll()
            for var i in 0..<(Data!.items![indexPath.row].cart_addons!.count)
            {
                let Result = Data!.items![indexPath.row].cart_addons![i]
                
                let str = "\(Result.addon_product?.addon?.name! ?? "")"
                
                addonsNameArr.append(str)
                
            }

            if Data!.items![indexPath.row].cart_addons!.count == 0 {
                cell.subTitleLabel.isHidden = true
            }else{
                cell.subTitleLabel.isHidden = false
                let addonsstr = addonsNameArr.joined(separator: ", ")
                cell.subTitleLabel.text = addonsstr
            }*/
            
           
            
            let selectedItem = Data?.items![indexPath.row]
            let productName = selectedItem?.product?.name
            let quantity1 = "\(selectedItem?.quantity ?? 0)"
            let currency = selectedItem?.product?.prices?.currency ?? "$"
            let priceStr: String! = String(describing: selectedItem?.product?.prices?.orignal_price ?? 0)
            let priceDouble: Double = Double(priceStr) ?? 0.0
            cell.titleLabel.text = "\(productName ?? "")(\(quantity1)x\(currency)\(String(format: "%.02f", priceDouble)))"
            
            let quantityStr = Double(selectedItem?.quantity ?? 0)
            let originalPrice = Double(selectedItem?.product?.prices?.orignal_price ?? 0)
            let value =   originalPrice * quantityStr
            cell.descriptionLabel.text = currency + String(format: " %.02f", Double(value))
            
            var addonsNameArr = [String]()
            addonsNameArr.removeAll()
            var addonPriceArr = [String]()
            addonPriceArr.removeAll()
            for i in 0..<(selectedItem?.cart_addons!.count ?? 0){
                let Result = selectedItem?.cart_addons![i]
                let totalQty = Int(selectedItem?.quantity ?? Int(0.00)) * Int(Result?.quantity ?? Int(0.00))
                let str = "\(Result?.addon_product?.addon?.name ?? "")(\(totalQty) x \(Double(Result?.addon_product?.price ?? 0.00).twoDecimalPoint))"
                let price = Double((totalQty)) * Double(Result?.addon_product?.price ?? 0.00)
                let priceStr = "$\(price.twoDecimalPoint)"
                addonsNameArr.append(str)
                addonPriceArr.append(priceStr)
            }
            let addonsstr = addonsNameArr.joined(separator: "\n")
            cell.subTitleLabel.text = addonsstr
            let addOnPriceStr = addonPriceArr.joined(separator: "\n")
            cell.addonAmountLbl.text = addOnPriceStr

            
            
            return cell

            
        }

        
    }
    
    
}
