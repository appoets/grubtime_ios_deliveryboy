//
//  WalletViewController.swift
//  Project
//
//  Created by Prem's on 18/10/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {

    @IBOutlet var walletImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var amountTxtFld: UITextField!
    @IBOutlet var fiftyBtn: UIButton!
    @IBOutlet var hundredBtn: UIButton!
    @IBOutlet var thousandBtn: UIButton!
    @IBOutlet var addAmountBtn: UIButton!
    
    private lazy var loader : UIView = {
           return createLottieLoader(in: UIApplication.shared.keyWindow ?? self.view)
       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        initialLoads()
    }
    
    func initialLoads() {
        
        mainView.layer.cornerRadius = 6.0
        fiftyBtn.layer.cornerRadius = 6.0
        hundredBtn.layer.cornerRadius = 6.0
        thousandBtn.layer.cornerRadius = 6.0
        addAmountBtn.layer.cornerRadius = 6.0
        
        //self.loader.isHidden = false
        self.presenter?.get(api: .earnings, data: nil)
    }
    
    
    @IBAction func amountBtnAction(_ sender: Any) {
        
        if let btn = sender as? UIButton{
            switch btn {
            case fiftyBtn:
                amountTxtFld.text = "50"
                break
            case hundredBtn:
                amountTxtFld.text = "100"
                break
            case thousandBtn:
                amountTxtFld.text = "1000"
                break
            default:
                break
            }
            
        }
    }
    
    @IBAction func addAmountAction(_ sender: Any) {
        
        let param = ["amount": amountTxtFld.text ?? ""]
        let jsonData = try? JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
        self.presenter?.post(api: .earnings, data: jsonData)
    }
    
}


extension WalletViewController: PostViewProtocol {
    
    func onError(api: Base, message: String, statusCode code: Int) {
        self.loader.isHidden = true
        self.loader.isHideInMainThread(true)
        print(message)
    }
    
    
    func getEarningsData(api: Base, data: EarningsEntity?) {
        
        self.loader.isHidden = true
    }
}
