//
//  TransactionViewController.swift
//  Project
//
//  Created by Prem's on 18/10/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController {

    @IBOutlet var transactionTableView: UITableView!
    
    var transactionArray: [EarinigsData] = []
    
    private lazy var loader : UIView = {
        return createLottieLoader(in: UIApplication.shared.keyWindow ?? self.view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initialLoads()
    }
    
    func initialLoads() {
        
        let transactioNib = UINib(nibName: XIB.Names.TransactionTableViewCell, bundle: nil)
        transactionTableView.register(transactioNib, forCellReuseIdentifier: XIB.Names.TransactionTableViewCell)
        self.presenter?.get(api: .earnings, data: nil)
    }
    
}

extension TransactionViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return transactionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.TransactionTableViewCell) as! TransactionTableViewCell
        cell.updateCell(obj: transactionArray[indexPath.row])
        return cell
    }
    
}


extension TransactionViewController: PostViewProtocol {
    
    func onError(api: Base, message: String, statusCode code: Int) {
        self.loader.isHidden = true
        self.loader.isHideInMainThread(true)
        print(message)
    }

    
    func getEarningsData(api: Base, data: EarningsEntity?) {
        
        self.loader.isHidden = true
        self.transactionArray = data?.earnings ?? []
        self.transactionTableView.reloadData()
    }

}
