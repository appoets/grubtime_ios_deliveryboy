//
//  PaymentViewController.swift
//  Project
//
//  Created by Prem's on 18/10/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

     var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialLoads()
    }
    
    func initialLoads() {
        
        setNavigationcontroller()
        CapsPageMenu()
    }
    
    private func setNavigationcontroller(){
        let backBtn = UIButton(type: .custom)
        backBtn.setImage(UIImage(named: ImageString.imageValue.backicon), for: .normal)
        backBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backBtn.addTarget(self, action: #selector(backBtnAction), for: .touchUpInside)
        let backBtnItem = UIBarButtonItem(customView: backBtn)
        
        let termsBtn = UIButton(type: .custom)
        termsBtn.setTitle("Payment",for: .normal)
        termsBtn.titleLabel?.font = UIFont.semibold(size: 18)
        termsBtn.setTitleColor(UIColor.black, for: .normal)
        termsBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let termsBtnItem = UIBarButtonItem(customView: termsBtn)
        
        self.navigationItem.setLeftBarButtonItems([backBtnItem,termsBtnItem], animated: true)
    }
    
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }

    func CapsPageMenu(){
        
        var controllerArray : [UIViewController] = []
        
        let ongoingOrderVc : WalletViewController = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.WalletViewController) as! WalletViewController
        ongoingOrderVc.title = "Wallet"
        controllerArray.append(ongoingOrderVc)
        
        let pastOrderVc: TransactionViewController = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.TransactionViewController) as! TransactionViewController
        pastOrderVc.title = "Transaction"
        controllerArray.append(pastOrderVc)
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(UIColor.primary),
            .bottomMenuHairlineColor(UIColor.white),
            .menuItemFont(UIFont.regular(size: 14)),
            .menuHeight(50.0),
            .menuItemWidth(UIScreen.main.bounds.width/2),
            .selectedMenuItemLabelColor(UIColor.primary),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .enableHorizontalBounce(false)]
        
        let setrame = CGRect.init(x: 0.0, y: self.topbarHeight, width: self.view.frame.width, height: self.view.frame.height)
        
        
        
        
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame:setrame
            , pageMenuOptions: parameters)
        self.pageMenu?.delegate = self
        self.addChild(self.pageMenu!)
        self.view.addSubview(self.pageMenu!.view)
        
        self.pageMenu!.didMove(toParent: self)
    }
    
    

}

extension PaymentViewController: CAPSPageMenuDelegate{
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print(index)
        
    }
}
