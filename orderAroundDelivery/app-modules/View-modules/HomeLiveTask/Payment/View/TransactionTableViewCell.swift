//
//  TransactionTableViewCell.swift
//  Project
//
//  Created by Prem's on 18/10/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet var transcationIdLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!
    @IBOutlet var statusLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(obj: EarinigsData)  {
        
        transcationIdLbl.text = "\(obj.id ?? 0)"
        amountLbl.text = "$\(obj.amount ?? "")"
        statusLbl.text = obj.status ?? ""
    }
    
}
