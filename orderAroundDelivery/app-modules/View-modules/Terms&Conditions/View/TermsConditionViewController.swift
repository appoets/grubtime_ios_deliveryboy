//
//  TermsConditionViewController.swift
//  Project
//
//  Created by CSS on 02/02/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit
import WebKit

enum TermsWebViewType{
    case terms
    case privacyPolicy
    case bankDetail
}

class TermsConditionViewController: UIViewController {

    @IBOutlet var termsWebView: WKWebView!
    
    var viewType: TermsWebViewType = .terms
    
    private lazy var  loader = {
        return createActivityIndicator(self.view.window ?? self.view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        termsWebView.uiDelegate = self
        termsWebView.navigationDelegate = self
        initalLoads()
        self.loader.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.loader.isHidden = true
    }
    
    private func initalLoads(){
        setNavigationcontroller()
        callwebView()
        
    }
    
    // navigation
    private func setNavigationcontroller(){
        let backBtn = UIButton(type: .custom)
        backBtn.setImage(UIImage(named: ImageString.imageValue.backicon), for: .normal)
        backBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backBtn.addTarget(self, action: #selector(OrderHistoryViewController.onbackAction), for: .touchUpInside)
        let backBtnItem = UIBarButtonItem(customView: backBtn)
        
        let termsBtn = UIButton(type: .custom)
        switch viewType{
        case .terms:
            termsBtn.setTitle(APPLocalize.localizestring.termsOfServiceTitle.localize(), for: .normal)
            break
        case .privacyPolicy:
            termsBtn.setTitle(APPLocalize.localizestring.privacyPolicyTitle.localize(), for: .normal)
            break
        case .bankDetail:
            termsBtn.setTitle("", for: .normal)
            break
        }
        termsBtn.titleLabel?.font = UIFont.semibold(size: 18)
        termsBtn.setTitleColor(UIColor.black, for: .normal)
        termsBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let termsBtnItem = UIBarButtonItem(customView: termsBtn)
        
        self.navigationItem.setLeftBarButtonItems([backBtnItem,termsBtnItem], animated: true)
    }
    //back Button Action
    @objc func onbackAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func callwebView(){
        var myURLString = ""
        
        switch viewType {
        case .terms:
            myURLString = termsUrl
        case .privacyPolicy:
            myURLString = privacyPolicyURL
        case .bankDetail:
            myURLString = "https://connect.stripe.com/express/oauth/authorize?response_type=code&client_id=ca_GCqVbp3vX9JZsAk0XeOs8aWraIUCgFOA&scope=read_write&redirect_uri=https://oyola.co/transporter/stripe/connect/callback"
        }
        
        
        if let url = URL(string: myURLString){
            let request = URLRequest(url: url)
            termsWebView.load(request)
        }
        
    }
}

extension TermsConditionViewController: WKNavigationDelegate, WKUIDelegate{
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
        print(error.localizedDescription)
        self.loader.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        print("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        self.loader.isHidden = true
         print("finish to load")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString {
            //urlStr is what you want
            
            if urlStr.starts(with: "\(baseUrl)/api/transporter/stripe/connect?code="){
                
                let removeText = "\(baseUrl)/api/transporter/stripe/connect?code="
                
                let token = urlStr.dropFirst(removeText.count)
                
                var params = [String: Any]()
                params["code"] = token
                let jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                self.presenter?.post(api: .stripeToken, data: jsonData)
                self.loader.isHidden = false
            }
        }

        decisionHandler(.allow)
    }
}

extension TermsConditionViewController: PostViewProtocol {
    
    func onError(api: Base, message: String, statusCode code: Int) {
        self.loader.isHidden = true
    }
    
    func getStripeTokenData(api: Base, data: StripeTokenEntity?) {
        self.loader.isHidden = true
        
        if let responseData = data{
            if responseData.status ?? false{
                let alert = showAlert(message: responseData.message)
                self.present(alert, animated: true, completion: nil)
            }else{
                let alert = showAlert(message: responseData.message)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

}
